import java.util.Scanner;
import java.util.ArrayList;

public class SpiderSolitaire
{
    /** Number of stacks on the board **/
    public final int NUM_STACKS = 7;

    /** Number of complete decks used in this game.  A 1-suit deck, which is the
     *  default for this lab, consists of 13 cards (Ace through King).
     */
    public final int NUM_DECKS = 4;

    /** A Board contains stacks and a draw pile **/
    private Board board;

    /** Used for keyboard input **/
    private Scanner input;

    public SpiderSolitaire()
    {
        // Start a new game with NUM_STACKS stacks and NUM_DECKS of cards
        board = new Board(NUM_STACKS, NUM_DECKS);
        input = new Scanner(System.in);
    }
    

    /** Main game loop that plays games until user wins or quits **/
    public void play() {

        board.printBoard();
        boolean gameOver = false;

        while(!gameOver) {
            System.out.println("\nCommands:");
            System.out.println("   move [card] [source_stack] [destination_stack]");
            System.out.println("   draw");
            System.out.println("   clear [source_stack]");
            System.out.println("   restart");
            System.out.println("   save");
            System.out.println("   load");
            System.out.println("   quit");
            System.out.print(">");
            String command = input.next();

            if (command.equals("move")) {
                String symbol = input.next();
                ArrayList<String> symbols = new ArrayList<>();
                symbols.add("A");
                symbols.add("2");
                symbols.add("3");
                symbols.add("4");
                symbols.add("5");
                symbols.add("6");
                symbols.add("7");
                symbols.add("8");
                symbols.add("9");
                symbols.add("T");
                symbols.add("J");
                symbols.add("Q");
                symbols.add("K");
                if(symbols.indexOf(symbol) != -1){
                    if(input.hasNextInt()){
                        int sourceStack = input.nextInt();
                        if(input.hasNextInt()){
                            int destinationStack = input.nextInt();
                            board.makeMove(symbol, sourceStack - 1, destinationStack - 1);
                        }else{
                            System.out.println("Invalid command.");
                        }
                    }else{
                        System.out.println("Invalid command.");
                    }
                }
            }
            else if (command.equals("draw")) {
                board.drawCards();
            }
            else if (command.equals("clear")) {
                if(input.hasNextInt()){
                    int sourceStack = input.nextInt();
                    board.clear(sourceStack - 1);
                }else{
                   System.out.println("Invalid command."); 
                }
            }
            else if (command.equals("restart")) {
                board = new Board(NUM_STACKS, NUM_DECKS);
            }
            else if (command.equals("quit")) {
                System.out.println("Goodbye!");
                System.exit(0);
            }else if(command.equals("save")) {
                board.save();
            }
            else if(command.equals("load")) {
                board.load();
            }
            else {
                System.out.println("Invalid command.");
            }

            board.printBoard();

            // If all stacks and the draw pile are clear, you win!
            if (board.isEmpty()) {
                gameOver = true;
            }
        }
        System.out.println("Congratulations!  You win!");
    }
}
