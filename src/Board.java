import java.util.ArrayList;
import java.util.Random;
import java.io.FileWriter;
import javax.swing.JFileChooser;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
public class Board
{   
    /*
     * Varsha Vijay p5 12/17/20
     *At fist i made mistakes in when translating the board back from the save file
     *I also did not catch the Null pointer exeption 
     *they have been fixed
     */
    ArrayList<Deck>  stacks;
    Deck draw;
    /**
     *  Sets up the Board and fills the stacks and draw pile from a Deck
     *  consisting of numDecks Decks.  Here are examples:
     *  
     *  # numDecks     #cards in overall Deck
     *      1          13 (all same suit)
     *      2          26 (all same suit)
     *      3          39 (all same suit)
     *      4          52 (all same suit)
     *      
     *  Once the overall Deck is built, it is shuffled and half the cards
     *  are placed as evenly as possible into the stacks.  The other half
     *  of the cards remain in the draw pile.
     */    
    public Board(int numStacks, int numDecks) {
        String[] symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        stacks = new ArrayList<>();
        for(int i = 0; i < numStacks; i++){
            stacks.add(new Deck());
        }
        draw = new Deck();
        for(int n = 0; n < numDecks; n++){
            for(int i = 0; i < symbols.length; i++){
                draw.add(new Card(symbols[i],values[i], false));
            }
        }
        draw.shuffle();
        int d = 0;
        int l = draw.length()/2;
        for(int c = 0; c < l; c++){
            draw.fromTo(0, stacks.get(d));
            if(d == numStacks - 1){
                d = 0;
            }else{ 
                d++;
            }
        }
        
        for(int i = 0; i < numStacks; i++){
            Deck dk =  stacks.get(i);
            dk.getCard(dk.length() -1).setFaceUp(true);
            
        }
        
        
    }

    /**
     *  Moves a run of cards from src to dest (if possible) and flips the
     *  next card if one is available.
     */
    public void makeMove(String symbol, int src, int dest) {
        int y = 0;
        boolean sym = false;
        Deck d = stacks.get(src);
        for(int i = d.length() -1; i>= 0; i--){
            if(d.getCard(i).getSymbol().equals(symbol)){
                y = i;
                sym = true;
                break;
            }
        }
        boolean valid = true;
        if(d.length() > 0){
            int val = d.getCard(y).getValue();
            for(int i = y + 1; i < d.length(); i++){
                if(d.getCard(i).getValue() != val - 1){
                    valid = false;
                    break;
                }
                val = d.getCard(i).getValue();
            }
        }else{
            valid = false;
        }
        Deck in;
        boolean run = true;
        if(dest < stacks.size()){
            in = stacks.get(dest);
            if(in.length() > 0 && d.length() > 0){
                if(in.getCard(in.length() - 1).getValue() != d.getCard(y).getValue()  + 1){
                    run = false;
                }
            }
        }else{
            run = false;
        }
        if(valid && sym && run){
            while(y < d.length()){
                d.fromTo(y , stacks.get(dest));
            }
            if(d.length() > 0){
                d.getCard(d.length() -1).setFaceUp(true);
            }
        }else{
            System.out.println("Invalid command.");
        }
    }

    /** 
     *  Moves one card onto each stack, or as many as are available
     */
    public void drawCards() {
        boolean empty = false;
        for(int i = 0; i < stacks.size(); i++){
            if(stacks.get(i).length() == 0){
                empty = true;
            }
        }
        for(int i = 0; i < stacks.size(); i++){
            if(draw.length() > 0 && !empty){
                draw.getCard(0).setFaceUp(true);
                draw.fromTo(0, stacks.get(i));
            }
        }
        if(empty){
            System.out.println("Invalid command.");
        }
    }

    /**
     *  Returns true if all stacks and the draw pile are all empty
     */ 
    public boolean isEmpty() {
        boolean empty = true;
        if(draw.length() > 0){
            empty = false;
        }else{
            for(int i = 0; i < stacks.size(); i++){
                if(stacks.get(i).length() != 0){
                    empty = false;
                }
            }
        }
        return empty;
    }

    /**
     *  If there is a run of A through K starting at the end of sourceStack
     *  then the run is removed from the game or placed into a completed
     *  stacks area.
     *  
     *  If there is not a run of A through K starting at the end of sourceStack
     *  then an invalid move message is displayed and the Board is not changed.
     */
    public void clear(int sourceStack) {
        boolean clear = true;
        Deck d = new Deck();
        if(sourceStack < stacks.size() ){
            d = stacks.get(sourceStack);
        }else{
            clear = false;
        }
        int v = 1;
        if(d.length() >= 13){
            for(int i = d.length() -1; i>= d.length() - 13; i--){
                if(!d.getCard(i).isFaceUp() && d.getCard(i).getValue() != v){
                    clear = false;
                }
                v++;
            }
            
        }else{
            clear = false;
        }
        if(clear){
            for(int i =  d.length() -1;i>= d.length() - 13; i--){
                d.remove(i);
            }
        }else{
            System.out.println("Invalid command.");
        }
        
    }

    /**
     * Prints the board to the terminal window by displaying the stacks, draw
     * pile, and done stacks (if you chose to have them)
     */
    public void printBoard() {
       System.out.println("Stacks:");
       for(int i = 0; i < stacks.size(); i++){
           System.out.print( i+1 +": " + stacks.get(i).toString() + "\n" );
       }
       System.out.println("Draw Pile");
       System.out.println(draw.toString());
    }
    
    public void save(){
        JFileChooser chooser = new JFileChooser(".");
        chooser.showSaveDialog(null);
        File apple = chooser.getSelectedFile();
        try{
            FileWriter banana = new FileWriter(apple);
            String ing = "";
            ing = ing + stacks.size() + " ";
            for(int i  = 0; i < stacks.size(); i++){
                ing = ing + stacks.get(i).getState() + " ";
            }
            ing = ing + draw.getState();
            banana.write(ing, 0, ing.length());
            banana.close();
        }catch(IOException i){
            System.out.println("Error: " + i.getMessage());
        }catch(NullPointerException n){
            System.out.println("Error: " + n.getMessage());
        }
    }
    
    public void load(){
        JFileChooser chooser = new JFileChooser(".");
        chooser.showOpenDialog(null);
        File apple = chooser.getSelectedFile();
        Scanner in;
        try{
            in = new Scanner(apple);
            int nums = 0;
            if(in.hasNextInt()){
                stacks = new ArrayList<>();
                nums = in.nextInt();
            }
            for(int i = 0; i < nums; i++){
                String ing = in.next();
                Deck d = new Deck(ing);
                stacks.add(d);
            }
            draw = new Deck(in.next());
        }catch(IOException i){
            System.out.println("Error: " + i.getMessage());
        }catch(NullPointerException n){
            System.out.println("Error: " + n.getMessage());
        }
    }
}