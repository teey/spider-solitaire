/**
 * Card.java
 *
 * <code>Card</code> represents a basic playing card.
 */
public class Card implements Comparable<Card>
{
   
    /** String value that holds the symbol of the card.
    Examples: "A", "Ace", "10", "Ten", "Wild", "Pikachu"
     */
    private String symbol;

    /** int value that holds the value this card is worth */
    private int value;

    /** boolean value that determines whether this card is face up or down */
    private boolean isFaceUp;

    /**
     * Creates a new <code>Card</code> instance.
     *
     * @param symbol  a <code>String</code> value representing the symbol of the card
     * @param value an <code>int</code> value containing the point value of the card
     * @param isUp a <code>boolean</code> value representing the state of the card
     */    
    public Card(String symbol, int value) {
        this.symbol = symbol;
        this.value = value;
        isFaceUp = false;
    }
    
    public Card(String symbol, int value, boolean f) {
        this.symbol = symbol;
        this.value = value;
        isFaceUp = f;
    }
    /**
     * Getter method to access this <code>Card</code>'s symbol.
     * 
     * @return this <code>Card</code>'s symbol.
     */
    public String getSymbol() {
        return symbol;
    }
    /**
     * Getter method to access this <code>Card</code>'s value.
     * 
     * @return this <code>Card</code>'s value.
     */
    public int getValue() {
        return value;
    }
    /**
     * Returns whether this <code>Card</code> is face up or face down.
     * 
     * @return true if this <code>Card</code> is face up and false if face down.
     */
    public boolean isFaceUp() {
        return isFaceUp;
    }
    /**
     * Sets the state of the <code>Card</code> to face up or face down
     * @param state if true <code>Card</code> is face up. if false card is face down
     */
    public void setFaceUp(boolean state) {
        isFaceUp = state;
    }

    /**
     * Returns whether or not this <code>Card</code> is equal to another
     *  
     *  @return whether or not this <code>Card</code> is equal to other.
     */
    public boolean equals(Card other) {
        if(value == other.getValue()){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Compares this <code>Card</code> to another and returns their difference
     * @return the difference between this <code>Card</code> and another
     */
    @Override
    public int compareTo(Card c){
        int diff = c.getValue() - value;
        if(value < 0){
            return - diff;
        }else{
            return diff;
        }
    }
    /**
     * Returns this card as a String.  If the card is face down, "X"
     * is returned.  Otherwise the symbol of the card is returned.
     *
     * @return a <code>String</code> containing the symbol or and X,
     * depending on whether the card is face up or down.
     */
    @Override
    public String toString() {
        if(isFaceUp){
            return symbol;
        }else{
            return "X";
        }
    }
}
