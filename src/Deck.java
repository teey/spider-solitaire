import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
public class Deck
{
    /*
     * Varsha Vijay
     * I was not sure what methods to include. 
     */
    ArrayList <Card> d;
    
    public Deck(){
        d = new ArrayList<>();
    }
    
    public Deck(String state){
        d = new ArrayList<>();
        ArrayList<String> symbols = new ArrayList<>();
        symbols.add("A");
        symbols.add("2");
        symbols.add("3");
        symbols.add("4");
        symbols.add("5");
        symbols.add("6");
        symbols.add("7");
        symbols.add("8");
        symbols.add("9");
        symbols.add("T");
        symbols.add("J");
        symbols.add("Q");
        symbols.add("K");
        
        
        for(int i = 0; i < state.length(); i++){
            String str = (state.substring(i, i+1));
            int val = symbols.indexOf(str) + 1;
            i++;
            boolean s = false;
            if(state.substring(i, i+1).equals("t")){
                s = true;
            }
            Card c = new Card(str, val, s);
            d.add(c);
        }
    }
    public void add(Card c){
        d.add(c);
    }
    
    public Card getCard(int i){
        return d.get(i);
    }
    public Card removeCard(int i){
        Card c = d.remove(i);
        return c;
    }
    public void remove(int i){
        d.remove(i);
    }
    
    public void shuffle(){
        ArrayList<Card> copy = new ArrayList<>();
        for(int i = 0; i <d.size(); i++){
            copy.add(d.get(i));
        }
        ArrayList<Card> perms = new ArrayList<>();
        while(copy.size() > 0){
            Random r = new Random();
            int i = r.nextInt(copy.size());
            perms.add(copy.get(i));
            copy.remove(i);
        }
        d = perms;
    }
    
    @Override
    public String toString(){
        String ing = "[";
        for(int i = 0; i <d.size(); i++){
            if(i == 0){
                ing = ing + d.get(i).toString() + ",";
            }else if (i + 1 == d.size()){
                ing = ing +  " " + d.get(i).toString();
            }else{
                ing = ing +  " " + d.get(i).toString() + ",";
            }
        }
        ing = ing + "]";
        return ing;
    }
    
    public int length(){
       return d.size(); 
    }
    public void fromTo(int i, Deck d){
        Card c = this.removeCard(i);
        d.add(c);
    }
    public String getState(){
        String ing = "";
        for(int i = 0; i < length(); i++){
            Card c = getCard(i);
            ing = ing + c.getSymbol();
            if(c.isFaceUp()){
                ing = ing + "t";
            }else{
                ing = ing + "f";
            }
        }
        return ing;
    }
    public void sort() {
    	for(int i = 0; i < d.size(); i ++) {
			int windex = i ;
			for(int y = 1 + i ; y < d.size(); y++) {
				if(d.get(y).compareTo(d.get(windex)) > 0) {
					windex = y;
				}
			}
			Card z = d.get(i);
			d.set(i, d.get(windex));
			d.set(windex, z);
			System.out.println(d);
		}
    }
}
