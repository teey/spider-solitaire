public class DeckTester
{
    public static void main(String[] args) {
        String[] symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        Deck deck = new Deck();
        for(int i = 0; i < symbols.length; i++){
            deck.add(new Card(symbols[i], values[i]));
        }
        for(int i = 0; i < deck.length(); i++){
            if(i%2 == 0){
                deck.getCard(i).setFaceUp(true);
            }
        }
        String ing = deck.toString();
        System.out.println(ing);
        String s = deck.getState();
        Deck d = new Deck(s);
        System.out.println(d.toString());
        /* deck.shuffle();
        System.out.println(deck.toString());
        System.out.println(deck.getCard(0).isFaceUp());
        deck.removeCard(0);
        System.out.println(deck.toString());
        Card c = deck.getCard(0);
        System.out.println(deck.getCard(0).toString());
        deck.add(c);
        System.out.println(deck.toString());
        */
    }
}
